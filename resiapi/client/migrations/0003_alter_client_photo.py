# Generated by Django 4.0.4 on 2022-04-17 00:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0002_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='client',
            name='photo',
            field=models.ImageField(null=True, upload_to=''),
        ),
    ]
