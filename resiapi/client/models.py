from django.db import models
from django.db.models import Field


# Create your models here.
@Field.register_lookup
class NotEqual(models.Lookup):
    lookup_name = 'ne'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params

class Client(models.Model):
    nom = models.CharField(null=False, max_length=25)
    prenoms = models.CharField(null=False, max_length=75)
    phone = models.CharField(null=False, max_length=10)
    photo = models.ImageField(null=True)
    username = models.CharField(null=False, max_length=255, unique=True)
    user_id = models.ForeignKey('User.CustomUser', null=True, on_delete=models.CASCADE)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
class Commande(models.Model):
    
    CHOICES = (
        ('ATTENTE', 'En Attente'),
        ('VALIDE', 'Validé'),
        ('ANNULE', 'Annulé'),
    )
    
    idclient = models.ForeignKey(Client, null=False, on_delete=models.CASCADE)
    idresidence = models.ForeignKey('proprio.Residence', null=False, on_delete=models.CASCADE)
    prixactuel = models.IntegerField(null=False)
    datedebut = models.DateField(auto_now=False, auto_now_add=False)
    datefin = models.DateField(auto_now=False, auto_now_add=False)
    versementduclient = models.BooleanField(default=False)
    cleauclient = models.BooleanField(default=False)
    versementauproprio = models.BooleanField(default=False)
    datecommande = models.DateTimeField(auto_now_add=True)
    statucommande = models.CharField(null=False, max_length=100, choices = CHOICES)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
    class Meta:
        ordering = ('-datecommande',)
    
class AjoutDeSejour(models.Model):
    
    CHOICES = (
        ('ATTENTE', 'En Attente'),
        ('VALIDE', 'Validé'),
        ('ANNULE', 'Annulé'),
    )
    
    idcommande = models.ForeignKey(Commande, null=False, on_delete=models.CASCADE)
    datefin = models.DateField(null=True, auto_now=False, auto_now_add=False)
    versementduclient = models.BooleanField(default=False)
    dateajout = models.DateTimeField(auto_now_add=True)
    statudemande = models.CharField(null=False, max_length=100, choices = CHOICES)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

class NoteResidence(models.Model):
    
    CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (3, '4'),
        (5, '5'),
    )
    
    idclient = models.ForeignKey(Client, null=False, on_delete=models.CASCADE)
    idresidence = models.ForeignKey('proprio.Residence', null=False, on_delete=models.CASCADE)
    note = models.IntegerField(null=False, choices = CHOICES)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
class ResiView (models.Model):
    
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, db_column='id')
    nbpieces = models.IntegerField(null=False, db_column='nbpieces')
    descriptifresidence = models.TextField(null=False, db_column='descriptifresidence')
    ville = models.CharField(null=False, max_length=100, db_column='ville')
    quartier = models.CharField(null=False, max_length=100, db_column='quartier')
    prixjournalier = models.IntegerField(null=False, db_column='prixjournalier')
    disponibilite = models.BooleanField(default=True, db_column='disponibilite')
    photocouverture = models.ImageField(null=True, db_column='photocouverture')
    idproprio = models.ForeignKey('proprio.Proprietaire', on_delete=models.CASCADE, null=False, db_column='idproprio_id')
    nbvote = models.IntegerField(db_column='nbvote')
    moyenne = models.DecimalField(max_digits=3, decimal_places=2,db_column='moyenne')
    createdAt = models.DateTimeField(auto_now_add=True, db_column='createdAt')
    updatedAt = models.DateTimeField(auto_now=True, db_column='updatedAt')

    class Meta:
        managed = False
        db_table ='resiview'
        ordering = ('-moyenne',)
        
class DispoView (models.Model):
    id = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, db_column='id')
    idresi = models.IntegerField(db_column='idresi')
    datedebut = models.DateField(auto_now=False, auto_now_add=False, db_column='datedebut')
    datefin = models.DateField(auto_now=False, auto_now_add=False, db_column='datefin')
    
    class Meta:
        managed = False
        db_table ='dispoview'
        
class HistoCmdClientView (models.Model):
    
    CHOICES = (
        ('ATTENTE', 'En Attente'),
        ('VALIDE', 'Validé'),
        ('ANNULE', 'Annulé'),
    )
    
    idcmd = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, db_column='idcmd')
    prixactuel = models.IntegerField(null=False, db_column='prixactuel')
    datedebut = models.DateField(auto_now=False, auto_now_add=False, db_column='datedebut')
    datefin = models.DateField(auto_now=False, auto_now_add=False, db_column='datefin')
    datecommande = models.DateTimeField(auto_now_add=True, db_column='datecommande')
    statucommande = models.CharField(null=False, max_length=100, choices = CHOICES, db_column='statucommande')
    idclient = models.IntegerField(db_column='idclient')
    idresi = models.IntegerField(db_column='idresi')
    nbpieces = models.IntegerField(null=False, db_column='nbpieces')
    descriptifresidence = models.TextField(null=False, db_column='descriptifresidence')
    ville = models.CharField(null=False, max_length=100, db_column='ville')
    quartier = models.CharField(null=False, max_length=100, db_column='quartier')
    photocouverture = models.ImageField(null=True, db_column='photocouverture')
    
    class Meta:
        managed = False
        db_table ='histocmdclientview'
