from functools import partial
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import AuthenticationFailed
from .serializers import UserSerializer
from .models import CustomUser
import jwt, datetime
from rest_framework.permissions import AllowAny
import requests, json
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework import status
from rest_framework_simplejwt.tokens import RefreshToken
from django.contrib.auth.models import Group
from django.contrib.auth.hashers import make_password
from django.db import connection


# Create your views here.
class RegisterClientView(APIView):
    permission_classes = [AllowAny]
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user = CustomUser.objects.filter(username=request.data['username']).first()
        id = user.id
        user = CustomUser.objects.get(id=id)
        client=Group.objects.get(name='client')
        client.user_set.add(user)
        return Response(serializer.data)

class RegisterProprioView(APIView):
    permission_classes = [AllowAny]
    def post(self, request):
        serializer = UserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        user = CustomUser.objects.filter(username=request.data['username']).first()
        user.is_active = False
        user.save()
        id = user.id
        user = CustomUser.objects.get(id=id)
        client=Group.objects.get(name='proprio')
        client.user_set.add(user)
        return Response(serializer.data)
    
class ChangePassword(APIView):
    permission_classes = [AllowAny]
    def post(self, request, **kwargs):
        user = CustomUser.objects.get(id=kwargs['id'])
        if user is None:
            raise AuthenticationFailed('User not found!')
        password = request.data['password']
        
        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect password!')
        
        password = make_password(request.data['newpassword'])
        
        serializer = UserSerializer(user, data={'password': password}, partial = True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(serializer.data)
        
    

class LoginView(APIView):
    permission_classes = [AllowAny]
    def post(self, request):
        username = request.data['username']
        password = request.data['password']

        user = CustomUser.objects.filter(username=username).first()

        if user is None:
            raise AuthenticationFailed('User not found!')

        if not user.check_password(password):
            raise AuthenticationFailed('Incorrect password!')

        refresh = RefreshToken.for_user(user)
        json_data = []
        json_data.append({
            'id': user.id,
            'refresh': str(refresh),
            'access': str(refresh.access_token),
        })
        return Response(json_data, status=status.HTTP_200_OK)



class LogoutView(APIView):

    def post(self, request):
        try:
            refresh_token = request.data["refresh_token"]
            token = RefreshToken(refresh_token)
            token.blacklist()

            return Response(status=status.HTTP_205_RESET_CONTENT)
        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)
