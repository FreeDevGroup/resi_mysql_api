# Generated by Django 4.0.4 on 2022-05-06 19:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0012_histocmdclientview'),
    ]

    operations = [
        migrations.RunSQL(
            "CREATE VIEW histocmdresiproprioview AS SELECT proprio.id AS idproprio, cmde.id AS idcommande, cmde.prixactuel, cmde.datedebut, cmde.datefin, cmde.versementduclient, cmde.cleauclient, cmde.versementauproprio, cmde.datecommande, cmde.statucommande,  resi.id AS idresidence, resi.nbpieces, resi.descriptifresidence, resi.ville, resi.quartier, resi.prixjournalier, resi.disponibilite, resi.photocouverture, cli.id AS idclient, cli.nom, cli.prenoms, cli.phone, cli.photo AS photoclient FROM client_commande cmde INNER JOIN proprio_residence resi ON cmde.idresidence_id = resi.id INNER JOIN proprio_proprietaire proprio ON resi.idproprio_id = proprio.id INNER JOIN client_client cli ON cmde.idclient_id = cli.id"
        )
    ]
