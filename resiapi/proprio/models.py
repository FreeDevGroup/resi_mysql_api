from django.db import models
from django.db.models import Field


# Create your models here.
@Field.register_lookup
class NotEqual(models.Lookup):
    lookup_name = 'ne'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return '%s <> %s' % (lhs, rhs), params


class Proprietaire(models.Model):
    nom = models.CharField(null=False, max_length=25)
    prenoms = models.CharField(null=False, max_length=75)
    phone = models.CharField(null=False, max_length=10)
    photo = models.ImageField(null=True)
    username = models.CharField(null=False, max_length=255, unique=True)
    user_id = user_id = models.ForeignKey('User.CustomUser', null=True, on_delete=models.CASCADE)
    piece_identite = models.ImageField(null=True)
    isactivate = models.BooleanField(default=False)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
class Residence(models.Model):
    idproprio = models.ForeignKey(Proprietaire, on_delete=models.CASCADE, null=False)
    nbpieces = models.IntegerField(null=False)
    descriptifresidence = models.TextField(null=False)
    ville = models.CharField(null=False, max_length=100)
    quartier = models.CharField(null=False, max_length=100)
    prixjournalier = models.IntegerField(null=False)
    disponibilite = models.BooleanField(default=True)
    photocouverture = models.ImageField(null=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    

class Piecesresi(models.Model):
    idresidence = models.ForeignKey(Residence, null=False, on_delete=models.CASCADE)
    nompiece = models.CharField(null=False, max_length=100)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)

class Imagepieceresi(models.Model):
    idpiece = models.ForeignKey(Piecesresi, null=False, on_delete=models.CASCADE)
    image = models.ImageField(null=False)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
class Historiqueresi(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    idresidence = models.ForeignKey(Residence, null=False, on_delete=models.CASCADE)
    idclient = models.ForeignKey('client.Client', on_delete=models.CASCADE, null=False)
    tempssurannonce = models.IntegerField(null=False)
    visite3D = models.BooleanField(default=False)
    residencecommandé = models.BooleanField(default=True)
    createdAt = models.DateTimeField(auto_now_add=True)
    updatedAt = models.DateTimeField(auto_now=True)
    
class HistoriqueCommandeResiView(models.Model):
    
    CHOICES = (
        ('ATTENTE', 'En Attente'),
        ('VALIDE', 'Validé'),
        ('ANNULE', 'Annulé'),
    )
    
    idproprio = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, db_column='idproprio')
    idcommande = models.IntegerField(db_column='idcommande')
    prixactuel = models.IntegerField(null=False, db_column='prixactuel')
    datedebut = models.DateField(auto_now=False, auto_now_add=False, db_column='datedebut')
    datefin = models.DateField(auto_now=False, auto_now_add=False, db_column='datefin')
    versementduclient = models.BooleanField(db_column='versementduclient')
    cleauclient = models.BooleanField(db_column='cleauclient')
    versementauproprio = models.BooleanField(db_column='versementauproprio')
    datecommande = models.DateTimeField(auto_now_add=True, db_column='datecommande')
    statucommande = models.CharField(null=False, max_length=100, choices = CHOICES, db_column='statucommande')
    idresidence = models.IntegerField(db_column='idresidence')
    nbpieces = models.IntegerField(null=False, db_column='nbpieces')
    descriptifresidence = models.TextField(null=False, db_column='descriptifresidence')
    ville = models.CharField(null=False, max_length=100, db_column='ville')
    quartier = models.CharField(null=False, max_length=100, db_column='quartier')
    prixjournalier = models.IntegerField(null=False, db_column='prixjournalier')
    disponibilite = models.BooleanField(default=True, db_column='disponibilite')
    photocouverture = models.ImageField(null=True, db_column='photocouverture')
    idclient = models.IntegerField(db_column='idclient')
    nom = models.CharField(null=False, max_length=25, db_column='nom')
    prenoms = models.CharField(null=False, max_length=75, db_column='prenoms')
    phone = models.CharField(null=False, max_length=10, db_column='phone')
    photoclient = models.ImageField(null=True, db_column='photoclient')
    
    class Meta:
        managed = False
        db_table ='histocmdresiproprioview'
        
class NbCommandeResiView(models.Model):
    idresidence = models.BigAutoField(auto_created=True, primary_key=True, serialize=False, db_column='idresidence')
    idproprio = models.IntegerField(db_column='idproprio')
    nbpieces = models.IntegerField(null=False, db_column='nbpieces')
    descriptifresidence = models.TextField(null=False, db_column='descriptifresidence')
    ville = models.CharField(null=False, max_length=100, db_column='ville')
    quartier = models.CharField(null=False, max_length=100, db_column='quartier')
    prixjournalier = models.IntegerField(null=False, db_column='prixjournalier')
    disponibilite = models.BooleanField(default=True, db_column='disponibilite')
    photocouverture = models.ImageField(null=True, db_column='photocouverture')
    nbcommande = models.IntegerField(null=False, db_column='nbcommande')
    
    class Meta:
        managed = False
        db_table ='nbcommanderesiview'
