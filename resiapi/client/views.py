from Resi_mysql.permissions import DjangoModelPermissionsStrict
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated, AllowAny
from User.models import CustomUser
from django_filters import rest_framework as filters
from django.db import connection

from client.models import *
from client.serializers import *


class ResiFilter(filters.FilterSet):
    class Meta:
        model = ResiView

        fields = {
            'prixjournalier': ['iexact', 'lte', 'gte'],
            'idproprio__id': ['iexact'],
            'ville': ['iexact'],
            'quartier': ['iexact'],
            'nbpieces' : ['iexact'],
        }

class HistoFilter(filters.FilterSet):
    class Meta:
        model = HistoCmdClientView

        fields = {
            'datecommande': ['lte', 'gte'],
            'statucommande': ['iexact', 'ne'],
            'idclient': ['iexact'],
            'ville': ['iexact'],
            'quartier': ['iexact'],
            'nbpieces' : ['iexact'],
        }

# Create your views here.
class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    
    def create(self, request):
        user = CustomUser.objects.filter(username=request.data['username']).first()
        mutable = request.POST._mutable
        request.POST._mutable = True
        request.POST['user_id'] = user.id
        request.POST._mutable = mutable
        serializer = ClientSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.data)

class CommandeViewSet(viewsets.ModelViewSet):
    queryset = Commande.objects.all()
    serializer_class = CommandeSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    http_method_names = ['get','post', 'patch', 'delete', 'head']
    filterset_fields = ['idclient', 'idresidence', 'statucommande']
    
    def partial_update(self, request, **kwargs):
        statu = request.data.get('statucommande', 'default')
        commande = Commande.objects.get(pk=kwargs['pk'])
        if statu == "VALIDE":
            with connection.cursor() as cursor:
                sql2 = "UPDATE client_commande SET statucommande = 'ANNULE' WHERE statucommande = 'ATTENTE' AND idresidence_id = %(idresidence)s AND datedebut <= %(fin)s AND datefin >= %(deb)s"
                params = {'idresidence': commande.idresidence_id, 'deb': commande.datedebut, 'fin': commande.datefin}
                cursor.execute(sql2, params=params)
        serializer = CommandeSerializer(commande, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
        return Response(serializer.data)
    
class AjoutViewSet(viewsets.ModelViewSet):
    queryset = AjoutDeSejour.objects.all()
    serializer_class = AjoutSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    filterset_fields = ['idcommande', 'statudemande']
    
class NoteViewSet(viewsets.ModelViewSet):
    queryset = NoteResidence.objects.all()
    serializer_class = NoteSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    filterset_fields = ['idclient' ,'idresidence']
    
class ResiViewViewSet(viewsets.ModelViewSet):
    queryset = ResiView.objects.all()
    serializer_class = ResiViewSerializer
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    filter_class = ResiFilter
    http_method_names = ['get', 'head']
    
class DispoViewViewSet(viewsets.ModelViewSet):
    queryset = DispoView.objects.all()
    serializer_class = DispoViewSerializer
    http_method_names = ['post', 'head']
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    
    def create(self, request):
        idresi = request.data['idresi']
        datedebut = request.data['datedebut']
        datefin = request.data['datefin']
        commandes = DispoView.objects.filter(idresi=idresi, datedebut__lte=datefin, datefin__gte=datedebut)
        serializer = DispoViewSerializer(commandes, many=True)
        return Response(serializer.data)
    
class HistoCmdClientViewViewSet(viewsets.ModelViewSet):
    queryset = HistoCmdClientView.objects.all()
    serializer_class = HistoCmdClientViewSerializer
    http_method_names = ['get', 'head']
    filter_class = HistoFilter
    permission_classes = (IsAuthenticated, DjangoModelPermissionsStrict)
    
